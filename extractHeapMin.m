function [mn,heap] = extractHeapMin(heap)
% extracts the minimum element from a heap (used for priority queues) (based on heapify)
%
% Remark: 1) -
%
% Time      : Ordo(logN)
%
% Syntax:
%   mx = extractHeapMin(heap)
%   Input params:
%      heap  - heap structure
%   Return values:
%      mn    - min element of the heap structure
%      heap  - remained heap
%
% Reference:
% (Cormen:alg)
%   Cormen et. al. Introduction to Algorithms, MIT Press, 2000,
%   Chapter 7.5, Priority Queues
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: -

% Copyright 2006-2007

% -- global consts --
global ERRMSG_HEAPUNDERFLOW;

% -- input checking --

% -- function body --

if length(heap) < 1
    error(ERRMSG_HEAPUNDERFLOW);
end;

lenH = length(heap);    % the last index of the heap

mne = inf;      % default min value
mnndx = lenH;   % default min index

% find the min element of the heap
for i=1:lenH
    if heap(i) < mne
        mne     = heap(i);
        mnndx   = i;
    end;
end;

% minimum element
mn = heap(mnndx);
% delete the min element from the heap
heap(mnndx) = [];   

%end