function Q = buildQ(adj,prior)
% builds priority queue structure from an adjacency matrix and priority list (based on heapify)
%
% Remark: 1) -
%
% Time      : Ordo(N)
%
% Syntax:
%   Q = buildQ(adj,prior)
%   Input params:
%      adj   - adjacency matrix
%      prior - priority list
%   Return values:
%      Q  - priority queue
%
% Reference: -
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: -

% Copyright 2006-2007

% -- input checking --

% -- function body --

% preallocation
Q = cell(size(adj,1),2);

% create priority queue Q as cell array without heap structure
for i=1:size(adj,1)
    Q{i,1}=i;   % index of nodes
    try
        Q{i,2}=prior(i);    % priority
    catch
        Q{i,2}=inf;         % default priority
    end;
end;

% structuring the Q
for i = fix(size(Q,1)/2):-1:1
    Q = heapifyQ(Q,i);
end;

%end