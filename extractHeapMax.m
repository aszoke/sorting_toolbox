function [mx,heap] = extractHeapMax(heap)
% extracts the maximum element from a heap (used for priority queues) (based on heapify)
%
% Remark: 1) -
%
% Time      : Ordo(logN)
%
% Syntax:
%   mx = extractHeapMax(heap)
%   Input params:
%      heap  - heap structure
%   Return values:
%      mx    - max element of the heap structure
%      heap  - remained heap
%
% Reference:
% (Cormen:alg)
%   Cormen et. al. Introduction to Algorithms, MIT Press, 2000,
%   Chapter 7.5, Priority Queues
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: -

% Copyright 2006-2007

% -- global consts --
global ERRMSG_HEAPUNDERFLOW;

% -- input checking --

% -- function body --

if length(heap) < 1
    error(ERRMSG_HEAPUNDERFLOW);
end;
% get the max value
mx = heap(1);
% 'heapify' the heap
heap(1) = heap(length(heap));
heap = heap(1:length(heap) - 1);
heap = heapify(heap,1);

%end