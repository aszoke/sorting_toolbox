function olist = heapSort(list)
% comparison-based sorting algorithm /by Williams/
%
% Description:
% Heapsort is a much more efficient version of selection sort. It also
% works by determining the largest (or smallest) element of the list, 
% placing that at the end (or beginning) of the list, then continuing with 
% the rest of the list, but accomplishes this task efficiently by using a 
% data structure called a heap, a special type of binary tree. Once the data 
% list has been made into a heap, the root node is guaranteed to be the 
% largest element. When it is removed and placed at the end of the list, 
% the heap is rearranged so the largest element remaining moves to the root. 
% Using the heap, finding the next largest element takes O(log n) time, 
% instead of O(n) for a linear scan as in simple selection sort. This allows 
% Heapsort to run in O(n log n) time.
%
% Remark: 1) METHOD: selection
%         2) STABILITY: no
%         3) SORT ALGORITHM TYPE: comparison
%         4) in place ordering 
%         5) integrates the benefits of insertion sort (space) and merge
%         sort (time)
%         5) DESIGN APPROACH: (binary) heap
%               - heap is like a binary tree
%               - heap property: list(parentOf(i)) >= list(i)
%
% Time      : Ordo(N*logN), Omega(N*logN)
% Space     : Ordo(1)
%
% Syntax:
%   list = heapSort(list)
%   Input params:
%      list  - orderable list
%   Return values:
%      list  - ordered list
%
% Reference:
% (Cormen:alg)
%   Cormen et. al. Introduction to Algorithms, MIT Press, 2000,
%   Chapter 7.4, The Heapsort Algorithm
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: -

% Copyright 2006-2007

% -- input checking --

% -- function body --
olist = zeros(size(list));

heap = buildHeap(list);
lheap = length(heap);
% NOTE: this algorithm a bit different as in the reference! 
% (see iteration end (1) and condition (length(heap) > 1))
for i=lheap:-1:1
    % put heap(1) into the 'olist' /root element, since root contains the 
    % largest element because of the 'heap property'/
    olist(i) = heap(1);
    % put the last element on the top of the root
    heap(1) = heap(i);
    if length(heap) > 1
        % decrease the size of the investigation, since the 'actual root' is in order
        heap = heap(1:length(heap)-1);
        heap = heapify(heap,1);         % the heap is getting more ordered!
    end;
end;

% end;