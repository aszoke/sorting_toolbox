function heap = buildHeap(list)
% builds heap structure from a list for heapSort and priority queues (based on heapify)
%
% Remark: 1) It goes through the remaining nodes of the tree and runs 'heapify' on each
%            node. The order of node quarantees that the subtrees rooted at children
%            of node 'i' are heaps before the 'heapify' is run at that node.
%
% Time      : Ordo(N)
%
% Syntax:
%   heap = buildHeap(list)
%   Input params:
%      list  - list structure
%   Return values:
%      heap  - heap structure
%
% Reference:
% (Cormen:alg)
%   Cormen et. al. Introduction to Algorithms, MIT Press, 2000,
%   Chapter 7.4, The Heapsort Algorithm
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: -

% Copyright 2006-2007

% -- input checking --

% -- function body --

for i = fix(length(list)/2):-1:1
    list = heapify(list,i);
end;
heap = list;

%end