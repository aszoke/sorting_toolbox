function [mn,Q] = extractQMin(Q)
% extracts the smallest priority (key) element from the priority queue
%
% Remark: 1) -
%
% Time      : Ordo(N)
%
% Syntax:
%   [mne,Q] = extractQMin(Q)
%   Input params:
%      Q     - priority queue
%   Return values:
%      mne   - min priority (key) element of the priority queue
%      Q     - remained priority queue
%
% Reference: -
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: -

% Copyright 2006-2007

% -- global consts --
global ERRMSG_QUEUEUNDERFLOW;

% -- input checking --

% -- function body --

if size(Q,1) < 1
    error(ERRMSG_QUEUEUNDERFLOW);
end;

lenQ = size(Q,1);    % the last index of the Q

mne = inf;      % default min value
mnndx = lenQ;   % default min index

% find the min element of the Q
for i=1:lenQ
    if Q{i,2} < mne
        mne     = Q{i,2};
        mnndx   = i;
    end;
end;

% minimum element
mn = Q(mnndx,:);
% delete the min element from the Q
Q(mnndx,:) = [];    

%end