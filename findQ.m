function indx = findQ(Q,item)
% finds an item in the priority queue
%
% Remark: 1) -
%
% Time      : Ordo(N)
%
% Syntax:
%   indx = findQ(item)
%   Input params:
%      Q     - priority Q
%      item  - item of the Q element
%   Return values:
%      indx  - item index of the Q element
%
% Reference: -
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: -

% Copyright 2006-2007

% -- input checking --

% -- function body --

indx = 0; % default index, means no item in the Q

for i=1:size(Q,1)
    if Q{i,1} == item
        indx = i;
    end;
end;

%end