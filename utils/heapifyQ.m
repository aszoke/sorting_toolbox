function Q = heapifyQ(Q,i)
% convert an Q to a heap in a bottom-up manner (the core algorithm of the priority queue)
%
% Remark: 1) the core algorithm of the priority queues
%         2) the same as HEAPIFY
%
% Time      : Ordo(logN)
%
% Syntax:
%   Q = heapifyQ(Q,i)
%   Input params:
%      Q     - orderable Q
%      i     - index of the heap
%   Return values:
%      Q     - ordered Q
%
% Reference: -
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: -

% Copyright 2006-2007

% -- input checking --

% -- function body --

l = 2*i;      % take the left child index
r = 2*i+1;    % take the right child index

% comparison with left child and select the largest
if l <= size(Q,1) && Q{l,2} > Q{i,2}
    largest = l;
else
    largest = i;
end;

% comparison the previous largest with the right child
% as a result largest = max(l,r,i)
if r <= size(Q,1) && Q{r,2} > Q{largest,2}
    largest = r;
end;

if ~(largest == i)                  % see: heapify()
    tmp = Q(i,:);                   
    Q(i,:) = Q(largest,:);          
    Q(largest,:) = tmp;
    Q = heapifyQ(Q,largest);
end;

%end

