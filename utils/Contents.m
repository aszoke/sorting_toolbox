% # Utils of the Sort Toolbox
%
% ### Features
% 
% * **heapify** - convert an array to a heap in a bottom-up manner (the core algorithm of the heapSort)
% * **heapifyQ** - convert an Q to a heap in a bottom-up manner (the core algorithm of the priority queue)
