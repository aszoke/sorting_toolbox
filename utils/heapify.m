function list = heapify(list,i)
% convert an array to a heap in a bottom-up manner (the core algorithm of the heapSort)
%
% Remark: 1) it is the core algorithm of the priority queues
%
% Time      : Ordo(logN)
%
% Syntax:
%   list = heapify(list,i)
%   Input params:
%      list  - orderable list
%      i     - index of the heap
%   Return values:
%      list  - ordered list
%
% Reference:
% (Cormen:alg)
%   Cormen et. al. Introduction to Algorithms, MIT Press, 2000,
%   Chapter 7.4, The Heapsort Algorithm
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: -

% Copyright 2006-2007

% -- input checking --

% -- function body --

l = 2*i;      % take the left child index
r = 2*i+1;    % take the right child index

% comparison with left child and select the largest
if l <= length(list) && list(l) > list(i)
    largest = l;
else
    largest = i;
end;

% comparison the previous largest with the right child
% as a result largest = max(l,r,i)
if r <= length(list) && list(r) > list(largest)
    largest = r;
end;

if ~(largest == i)
    tmp = list(i);
    list(i) = list(largest);
    list(largest) = tmp;
    list = heapify(list,largest);
end;

%end

