%% SORTING TOOLBOX - SAMPLE TEST SET
% Sorting algorithms are used to put elements of a list in a certain order.  Efficient sorting is important for optimizing the use of other algorithms which require input data to be in sorted lists.
%
% *Author*: Akos Szoke (aszoke@mit.bme.hu)
%
% *Web site*: <http://www.kese.hu>
%
% *Toolbox Source*: <https://bitbucket.org/aszoke/>
%

%%
dbstop if error;
clear;
globalConsts();

disp(' ***************************************************** ');
disp(' ********** SORTING ALGORITHMS TESTS: Start ********** ')
disp(' ***************************************************** ');


%% INSERTIONSORT
disp(' *** TEST INSERTIONSORT: Start *** ')
% example based on: 
%   Cormen et. al. Introduction to Algorithms, MIT Press, 2000, p. 5

% orderable list
l = [31 41 59 26 41 58]

ol = insertionSort(l)

disp(' *** TEST: Passed *** ')

%% MERGESORT
disp(' *** TEST MERGESORT: Start *** ')
% example based on: 
%   Cormen et. al. Introduction to Algorithms, MIT Press, 2000, p. 5

% orderable list
l = [31 41 59 26 41 58]

ol = mergeSort(l)

%% HEAPSORT
disp(' *** TEST HEAPSORT: Start *** ')
% example based on: 
%   Cormen et. al. Introduction to Algorithms, MIT Press, 2000, p. 146

% orderable list
l = [4 1 3 2 16 9 10 14 8 7]

ol = heapSort(l)

% priority list
lst = [4 1 3 2 16 9 10 14 8 7]
% create a heap from the list
heap = buildHeap(lst)

% extract the maximum element
[max,heap2] = extractHeapMax(heap)

%extract the minimum element
[mn,heap2] = extractHeapMin(heap)

e = 15; % insertable element
heap3 = insertElemToHeap(heap,e)

disp(' *** TEST: Passed *** ')

%%

disp(' *********************************************** ');
disp(' ********* SORTING ALGORITHMS TESTS: End   ********** ')
disp(' *********************************************** ');

