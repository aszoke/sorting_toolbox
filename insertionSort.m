function list = insertionSort(list)
% insertion sort algorithm
%
% Descrition:
% Insertion sort is a simple sorting algorithm that is relatively efficient
% for small lists and mostly-sorted lists, and often is used as part of more 
% sophisticated algorithms. It works by taking elements from the list one by 
% one and inserting them in their correct position into a new sorted list. 
% In arrays, the new list and the remaining elements can share the array's 
% space, but insertion is expensive, requiring shifting all following elements 
% over by one. The insertion sort works just like its name suggests - it 
% inserts each item into its proper place in the final list. The simplest 
% implementation of this requires two list structures - the source list and 
% the list into which sorted items are inserted. To save memory, most 
% implementations use an in-place sort that works by moving the current item 
% past the already sorted items and repeatedly swapping it with the preceding 
% item until it is in place. Shell sort is a variant of insertion 
% sort that is more efficient for larger lists. This method is much more 
% efficient than the bubble sort, though it has more constraints.
%
% Remark: 1) METHOD: insertion
%         2) STABILITY: yes
%         3) SORT ALGORITHM TYPE: comparison
%         2) running time depends on the entropy of the orderable list
%               (from Ordo(N)->Ordo(N^2))
%         3) in place ordering
%         4) uses linear search to scan (backward) through the sorted
%            subarray
%         5) DESIGN APPROACH: incremental
%               - having a sorted subarray list(1..j-1)
%               - we insert a single element list(j) into the proper place
%               in the revious subarray
%               - yielding an INCREMENTED sorted subarray list(1..j)
%
% Complexity: Ordo(N^2) /worst case/, Omega(N) /best case/
% Space     : Ordo(1) /constant/
%
% Syntax:
%   list = insertionSort(list)
%   Input params:
%      list  - orderable list
%   Return values:
%      list - ordered list
%
% Reference:
% (Cormen:alg)
%   Cormen et. al. Introduction to Algorithms, MIT Press, 2000,
%   Chapter 1.1, Algorithms
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: -

% Copyright 2006-2007

% -- input checking --

% -- function body --

for j = 2:length(list)
    key = list(j);
    % insert list(j) to the ordered sequence list(1..j-1)
    i = j - 1;
    while (i > 0) && (list(i) > key)    % it realises a LINEAR search
        list(i + 1) = list(i);
        i = i - 1;
    end;
    list(i + 1) = key;
end;
    
% end;