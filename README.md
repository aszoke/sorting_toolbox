# Sorting Toolbox

## About

Sorting Toolbox contains algorithms that are used to put elements of a list in a certain order.  Efficient sorting is important for optimizing the use of other algorithms which require input data to be in sorted lists.

**Author**: Akos Szoke <aszoke@mit.bme.hu>

**Web site**: [http://www.kese.hu](http://www.kese.hu)

**Toolbox Source**: [https://bitbucket.org/aszoke/](https://bitbucket.org/aszoke/)

### Features

  * **buildHeap** - builds heap structure from a list for heapSort and priority queues (based on heapify)
  * **buildQ** - builds priority queue structure from an adjacency matrix and priority list (based on heapify)
  * **extractHeapMax** - extracts the maximum element from a heap (used for priority queues) (based on heapify)
  * **extractHeapMin** - extracts the minimum element from a heap (used for priority queues) (based on heapify)
  * **extractQMin** - extracts the smallest priority (key) element from the priority queue
  * **findQ** - finds an item in the priority queue
  * **heapSort** - comparison-based sorting algorithm /by Williams/
  * **insertElemToHeap** - inserts a key element to a heap
  * **insertionSort** - insertion sort algorithm
  * **mergeSort** - comparison-based sorting algorithm /by John von Neumann, 1945/

**Note**: [globalConsts.m](./sorting_toolbox/src/master/globalConsts.m) initializes global constants

### Dependencies

-

## Usage

using the functions in the feature list

### Example

[test_sorting_toolbox.m](./sorting_toolbox/src/master/test_sorting_toolbox.m)

The generated log of the example can be found in
[test_sorting_toolbox.html](https://bitbucket.org/aszoke/sorting_toolbox/raw/master/html/test_sorting_toolbox.html).

**Note**: *Due to the Bitbucket HTML preview restriction (see [HTML rendering for generated doc](https://bitbucket.org/site/master/issue/6353/html-rendering-for-generated-doc)), in order to view the Test run samples in HTML, use should use the [GitHub & BitBucket HTML Preview](http://htmlpreview.github.io/) service. (Copy the link below and paste into the text box that can be found in the service.)*

### Test data

see them in the example

## License

(The MIT License)

Copyright (c) 2011 Akos Szoke

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the 'Software'), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
