function olist = mergeSort(list)
% comparison-based sorting algorithm /by John von Neumann, 1945/
%
% Description:
% Merge sort takes advantage of the ease of merging already sorted lists
% into a new sorted list. It starts by comparing every two elements (i.e., 1 with 2, then 3 with 4...) 
% and swapping them if the first should come after the second. It then merges each of the resulting 
% lists of two into lists of four, then merges those lists of four, and so on; until at last two lists 
% are merged into the final sorted list. Of the algorithms described here, this is the first that scales 
% well to very large lists, because its worst-case running time is O(n log n).
%
% Remark: 1) METHOD: merging
%         2) STABILITY: yes
%         3) SORT ALGORITHM TYPE: comparison
%         4) NOT in place ordering (requires more space than in place ordering)
%         5) DESIGN APPROACH: divide-and-conquer
%               - DIVIDE: break the problem into several subproblems that are similar 
%                 to the original problems but smaller in size
%               - CONQUER: solve the subproblems recursively
%               - COMBINE: combine subrpoblem solution to yield a solution
%                 to the original problem
%
% Complexity: Ordo(N*logN), Omega(N*logN)
% Space     : Ordo(N)
%
% Syntax:
%   olist = mergeSort(list)
%   Input params:
%      list  - orderable list
%   Return values:
%      olist - ordered list
%
% Reference:
% (Cormen:alg)
%   Cormen et. al. Introduction to Algorithms, MIT Press, 2000,
%   Chapter 1.3.1, Divine-and-conquer approach
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: -

% Copyright 2006-2007

% -- input checking --

% -- function body --
n = length(list);
if  n == 1
    olist = list;
else
    % middle point:
    m = fix(n / 2);                     % fix function: round toward zero
    left = mergeSort(list(1:m));        % sorting the first half
    right = mergeSort(list(m + 1:n));   % sorting the second half
    olist = merge(left,right);          % merging the two half
end;

% --------------------- SUBFUNCTION ----------------------------

function z = merge(left,right)
n = length(left); 
m = length(right); 
z = zeros(1,n + m);

ileft = 1;  % The index of the next left-value to select.
iright = 1; % The index of the next right-value to select.

for iz=1:(n+m)
    % Deteremine the iz-th value for the merged array
    if ileft > n
        % All done with left-values. Select the next right-value.
        z(iz) = right(iright); iright = iright+1;
    elseif iright > m
        % All done with right-values. Select the next left-value.
        z(iz) = left(ileft); ileft = ileft + 1;
    elseif left(ileft) <= right(iright)
        % The next left-value is less than or equal to the next right-value
        z(iz) = left(ileft); ileft = ileft + 1;
    else
        % The next right-value is less than the next left-value
        z(iz) = right(iright); iright = iright + 1;
    end
end

% end;