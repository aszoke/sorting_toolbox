function heap = insertElemToHeap(heap, key)
% inserts a key element to a heap
%
% Remark: 1) -
%
% Time      : Ordo(logN)
%
% Syntax:
%   heap = insertElemToHeap(heap, key)
%   Input params:
%      heap  - heap structure
%      key   - insertable key element
%   Return values:
%      heap  - new heap structure
%
% Reference:
% (Cormen:alg)
%   Cormen et. al. Introduction to Algorithms, MIT Press, 2000,
%   Chapter 7.5, Priority Queues
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: -

% Copyright 2006-2007

% -- input checking --

% -- function body --

% put the insertable element to the end of the list
heap = [heap,key];

i = length(heap);
while (i > 1) && (heap(i) > heap(fix(i/2))) % heap(fix(i/2)) -> parent of heap(i)
    heap(i) = heap(fix(i/2));
    heap(fix(i/2)) = key;
    i = fix(i/2);
end;

%end


